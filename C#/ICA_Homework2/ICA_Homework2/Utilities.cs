﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICA_Homework2
{
    public class Utilities
    {
        public double[,] StoDMatrix(string [,]Matrix)
        {
            double[,] DoubleMatrix = new double[Matrix.GetLength(0), Matrix.GetLength(1)];

            for(var i=0;i< Matrix.GetLength(0); i++)
            {
                for(var j=0;j< Matrix.GetLength(1); j++)
                {
                    DoubleMatrix[i,j] = Double.Parse(Matrix[i, j], CultureInfo.InvariantCulture); ;
                }
            }
            return DoubleMatrix;
            
        }

        public int Index()
        {
            string path = @"C:\Users\paulo\Desktop\Mestrado\ICA\Homework2\Info\TI0077_HW2_assignment\solubility\solTestX.txt";
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            string x = file.ReadLine();
            char[] delimiterChars = { ' ', '\t' };
            string[] words = x.Split(delimiterChars);

            for(var i = 0; i < words.Length; i++)
            {
                
                if (words[i] == "\"MolWeight\"")
                {
                    return i;
                }
            }

            return -1;

            
        }

        public string[] PredictorName()
        {
            string path = @"C:\Users\paulo\Desktop\Mestrado\ICA\Homework2\Info\TI0077_HW2_assignment\solubility\solTestX.txt";
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            string x =  file.ReadLine();
            char[] delimiterChars = { ' ',  '\t' };
            string[] words = x.Split(delimiterChars);
            return words;

        }

        
        public string[,] solTestXtrans()
        {
            string[,] Data = new string[316, 229];
            string path = @"C:\Users\paulo\Desktop\Mestrado\ICA\Homework2\Info\TI0077_HW2_assignment\solubility\solTestXtrans.txt";

            int i = 0, j = 0;
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            while ((line = file.ReadLine()) != null)
            {
                char[] delimiterChars = { ' ', ',', ':', '\t' };
                string[] words = line.Split(delimiterChars);
                j = 0;
                foreach (string s in words)
                {
                    string z = s;
                    string aux = "";
                    if (j == 0)
                    {
                        foreach (char x in z)
                        {
                            if (x != '\"')
                            {
                                aux += x;
                            }
                        }
                        Data[i, j] = aux;
                    }
                    else
                    {
                        Data[i, j] = s;
                    }
                    j++;
                }
                i++;

            }
            file.Close();


            return Data;
        }
        public void Changes()
        {
            Console.WriteLine("ouo viu");
        }

        public string[,] solTrainXtrans()
        {
            string path = @"C:\Users\paulo\Desktop\Mestrado\ICA\Homework2\Info\TI0077_HW2_assignment\solubility\solTrainXtrans.txt";
            string[,] Data = new string[951, 229];
            int i = 0, j = 0;
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            while ((line = file.ReadLine()) != null)
            {
                char[] delimiterChars = { ' ', ',', ':', '\t' };
                string[] words = line.Split(delimiterChars);
                j = 0;
                foreach (string s in words)
                {
                    string z = s;
                    string aux = "";
                    if (j == 0)
                    {
                        foreach (char x in z)
                        {
                            if (x != '\"')
                            {
                                aux += x;
                            }
                        }
                        Data[i, j] = aux;
                    }
                    else
                    {
                        Data[i, j] = s;
                    }
                    j++;
                }
                i++;

            }
            file.Close();
            return Data;
        }
        public string[,] TestMatrix()
        {
            string[,] Data = new string[316, 229];
            string path = @"C:\Users\paulo\Desktop\Mestrado\ICA\Homework2\Info\TI0077_HW2_assignment\solubility\solTestX.txt";

            int i = 0, j = 0;
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            while ((line = file.ReadLine()) != null)
            {
                char[] delimiterChars = { ' ', ',', ':', '\t' };
                string[] words = line.Split(delimiterChars);
                j = 0;
                foreach (string s in words)
                {
                    string z = s;
                    string aux = "";
                    if (j == 0)
                    {
                        foreach (char x in z)
                        {
                            if (x != '\"')
                            {
                                aux += x;
                            }
                        }
                        Data[i, j] = aux;
                    }
                    else
                    {
                        Data[i, j] = s;
                    }
                    j++;
                }
                i++;

            }
            file.Close();


            return Data;
        }
        public string[,] TrainMatrix()
        {
            string path = @"C:\Users\paulo\Desktop\Mestrado\ICA\Homework2\Info\TI0077_HW2_assignment\solubility\solTrainX.txt";            
            string[,] Data = new string[951,229];            
            int i = 0, j = 0;
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            while ((line = file.ReadLine()) != null)
            {               
                char[] delimiterChars = { ' ', ',', ':', '\t' };
                string[] words = line.Split(delimiterChars);
                j = 0;
                foreach (string s in words)
                {
                    string z = s;
                    string aux = "";
                    if (j == 0)
                    {                        
                        foreach (char x in z)
                        {
                            if (x != '\"')
                            {
                                aux += x;
                            }
                        }
                        Data[i, j] = aux;
                    }
                    else
                    {
                        Data[i, j] = s;
                    }                    
                    j++;                    
                }
                i++;

            }
            file.Close();
            return Data;
        }


        public string[,] Queporra()
        {
            string[,] Data = new string[316, 229];
            string path = @"C:\Users\paulo\Desktop\Mestrado\ICA\Homework2\Info\TI0077_HW2_assignment\solubility\solTestX.txt";

            int i = 0, j = 0;
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            while ((line = file.ReadLine()) != null)
            {
                char[] delimiterChars = { ' ', ',', ':', '\t' };
                string[] words = line.Split(delimiterChars);
                j = 0;
                foreach (string s in words)
                {
                    string z = s;
                    string aux = "";
                    if (j == 0)
                    {
                        foreach (char x in z)
                        {
                            if (x != '\"')
                            {
                                aux += x;
                            }
                        }
                        //Data[i, j] = aux;
                    }
                    else
                    {
                        //Data[i, j] = s;
                    }
                    j++;
                }
                Console.WriteLine(j);
                i++;

            }
            file.Close();


            return Data;
        }
    }
}
