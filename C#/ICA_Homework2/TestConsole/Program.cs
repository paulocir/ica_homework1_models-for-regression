﻿using ICA_Homework2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using Accord.Statistics;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Startingaa!!!!");
            
            Utilities util = new Utilities();
            string[,] Train1 = util.Queporra();
            string[,] Train  = util.TrainMatrix();
            string[,] Test = util.TestMatrix();
            string[,] TrainTransX = util.solTrainXtrans();
            string[,] TestTransX = util.solTestXtrans();
            double[,] DoubleTrain = util.StoDMatrix(Train);
            double[,] DoubleTest = util.StoDMatrix(Test);
            double[,] DoubleTrainTrans = util.StoDMatrix(TrainTransX);
            double[,] DoubleTestTrans = util.StoDMatrix(TestTransX);
            double[,] corr1 = Measures.Correlation(DoubleTrainTrans);
            string[] names = util.PredictorName();
            Console.WriteLine(util.Index());

            Console.ReadLine();
        }
    }
}
